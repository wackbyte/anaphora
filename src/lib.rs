//! # `anaphora`
//!
//! <!--
//! [![crates.io](https://img.shields.io/crates/v/anaphora.svg)](https://crates.io/crates/anaphora)
//! [![Dependency Status](https://deps.rs/crate/anaphora/0.1.0/status.svg)](https://deps.rs/crate/anaphora/0.1.0)
//! -->
//! [![Unlicense](https://img.shields.io/badge/license-Unlicense-blue.svg)](https://unlicense.org/)
//! [![Minimum Supported Rust Version is 1.61](https://img.shields.io/badge/MSRV-1.61-white.svg)](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field)
//!
//! A means of [anonymous recursion](https://en.wikipedia.org/wiki/Anonymous_recursion), named for the term *[anaphora](https://en.wikipedia.org/wiki/Anaphora_(linguistics))*.
//!
//! ## This is not desirable
//!
//! If you need recursion, it is preferable to **not** do it with closures like this.
//! This pattern does not translate into Rust very well.
//!
//! Consider using the usual [`fn`] items and passing your state in through parameters.
//!
//! ## Limitations
//!
//! Here is a list of limitations introduced by the restrictions of the implementation:
//!
//! - Variables cannot be captured by mutable reference.
//! - Closures cannot be generic.
//! - Only one parameter is supported.
//! - Dynamic dispatch must be used.
//! - [`Anaphor`] and [`Anaphoric`] do not implement the [`Fn`] traits.
//!
//! ## Workarounds
//!
//! ### Only one parameter
//!
//! Simply take a tuple as a parameter and wrap in another closure:
//!
//! ```
//! # use anaphora::anaphora;
//! let add = anaphora(|it, (x, y): (u32, u32)| {
//! 	if x == 0 {
//! 		y
//! 	} else if y == 0 {
//! 		x
//! 	} else {
//! 		it.call((x - 1, y + 1))
//! 	}
//! });
//!
//! let add = |x, y| add.call((x, y));
//!
//! assert_eq!(add(7, 6), 13);
//! ```
//!
//! ### No `Fn` trait implementations
//!
//! The solution here is the same. Wrap in a closure.
//!
//! ```
//! # use anaphora::anaphora;
//! let fibonacci = anaphora(|it, n: u32| {
//! 	if n < 2 {
//! 		n
//! 	} else {
//! 		it.call(n - 1) + it.call(n - 2)
//! 	}
//! });
//!
//! let maybe_n = Some(13);
//! let maybe_the_nth_fibonacci_number = maybe_n.map(|n| fibonacci.call(n));
//!
//! assert_eq!(maybe_the_nth_fibonacci_number, Some(233));
//! ```
//!
//! ## Similar crates
//!
//! The [`fix_fn`](https://crates.io/crates/fix_fn) crate does something similar to this one.

#![cfg_attr(not(any(doc, test)), no_std)]

use core::marker::PhantomData;

/// A reference to the function.
pub struct Anaphor<'it, T, R> {
	inner: &'it dyn Fn(Self, T) -> R,
}

impl<'it, T, R> Anaphor<'it, T, R> {
	/// Calls the function with the given argument, returning its output.
	pub fn call(self, arg: T) -> R {
		(self.inner)(self, arg)
	}
}

impl<'it, T, R> Copy for Anaphor<'it, T, R> {}

impl<'it, T, R> Clone for Anaphor<'it, T, R> {
	fn clone(&self) -> Self {
		*self
	}
}

/// A self-referencing function.
#[derive(Copy, Clone)]
#[must_use = "`Anaphoric` functions will do nothing unless called"]
pub struct Anaphoric<'it, F> {
	inner: F,
	marker: PhantomData<&'it ()>,
}

impl<'it, F> Anaphoric<'it, F> {
	/// Calls the function with the given argument, returning its output.
	pub fn call<T, R>(&'it self, arg: T) -> R
	where
		T: 'it,
		R: 'it,
		F: 'it + Fn(Anaphor<'it, T, R>, T) -> R,
	{
		(self.inner)(Anaphor { inner: &self.inner }, arg)
	}
}

/// Creates a new self-referencing function.
///
/// # Examples
///
/// ```
/// # use anaphora::anaphora;
/// let factorial = anaphora(|it, n: u32| {
///     if n == 0 {
///         1
///     } else {
///         n * it.call(n - 1)
///     }
/// });
///
/// assert_eq!(factorial.call(0), 1);
/// assert_eq!(factorial.call(1), 1);
/// assert_eq!(factorial.call(2), 2);
/// assert_eq!(factorial.call(3), 6);
/// assert_eq!(factorial.call(4), 24);
/// ```
pub const fn anaphora<'it, T, R, F>(f: F) -> Anaphoric<'it, F>
where
	T: 'it,
	R: 'it,
	F: 'it + Fn(Anaphor<'it, T, R>, T) -> R,
{
	Anaphoric {
		inner: f,
		marker: PhantomData,
	}
}
