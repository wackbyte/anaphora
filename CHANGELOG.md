# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

## [0.1.0] - 2023-08-30

### Added
- Minimum Supported Rust Version: 1.61
- `Anaphoric`, a self-referencing function container.
- `Anaphor`, an erased reference to the function.
- `anaphora`, used to create anaphoric functions.

[Unreleased]: https://gitlab.com/wackbyte/anaphora/-/compare/v0.1.0...trunk
[0.1.0]: https://gitlab.com/wackbyte/anaphora/-/tags/v0.1.0

[Keep a Changelog]: https://keepachangelog.com/en/1.0.0
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
