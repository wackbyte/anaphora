# `anaphora`

<!--
[![crates.io](https://img.shields.io/crates/v/anaphora.svg)](https://crates.io/crates/anaphora)
[![docs.rs](https://docs.rs/anaphora/badge.svg)](https://docs.rs/anaphora)
-->
[![Dependency Status](https://deps.rs/repo/gitlab/wackbyte/anaphora/status.svg)](https://deps.rs/repo/gitlab/wackbyte/anaphora)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/wackbyte/anaphora?branch=trunk)](https://gitlab.com/wackbyte/anaphora/-/pipelines)
[![Unlicense](https://img.shields.io/badge/license-Unlicense-blue.svg)](https://unlicense.org/)
[![Minimum Supported Rust Version is 1.61](https://img.shields.io/badge/MSRV-1.61-white.svg)](https://doc.rust-lang.org/cargo/reference/manifest.html#the-rust-version-field)

Anonymous recursion for closures.
